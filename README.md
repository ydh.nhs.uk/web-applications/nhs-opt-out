## Introduction
This is an application designed for use at Yeovil District Hospital, to check if 
patients are included in the National data opt-out. 
For more details see https://digital.nhs.uk/services/national-data-opt-out

## Usage
The application allows users to either enter a single NHS number, or to upload a file
that can contain multiple NHS numbers. Supported file formats are .txt, .csv, 
.docx and .xslx. The results will be returned in a simple table.

## Implementation
The application is written in C#/.NET Core. It was developed in Visual Studio 2019.
It is set up to use Windows Authentication.

## Testing
Unit tests and test files are in the NhsOptOut.Tests sub-project.
A CI pipeline has not been set up for this project - it's easier to build, run the tests
and then deploy directly from Visual Studio.

## Deployment notes
The project has been setup with a Web Deploy target, to publish directly to watchdog.
Note that your user and machine must be granted access to the IIS Management
Service, this can be done in IIS Manager.

## Configuration
The database connection strings are in appsettings.production.json (this is not in source control)

## Auditing
Any access to the application is written to the log file and also to the AuditLog table on maildog.

## Mirth channels
The relevant Mirth channels can be found on maildog:
*  National Opt Out - MESH Out. This collects an up-to-date list of patient numbers and sends them to MESH
*  National Opt Out - MESH In control. This logs the receipt of any .ctl files from MESH
*  National Opt Out - MESH In data. This takes the results and loads them into the MESHReturn table

