using Microsoft.AspNetCore.Http;
using NUnit.Framework;
using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;

namespace NhsOptOut.Tests
{
    [TestFixture]
    public class TextReaderTest
    {
        [Test]
        public async Task GetNhsNumbersFromText()
        {
            // Arrange
            var physicalFile = new FileInfo("TestFiles/test.txt");
            IFormFile formFile = physicalFile.AsMockIFormFile();

            // Act
            NhsOptOut.Util.TextReader tr = new NhsOptOut.Util.TextReader();
            List<string> result = await tr.GetNhsNumbers(formFile);

            // Assert
            Assert.That(result.Count, Is.EqualTo(5));
            Assert.That(result.Contains("3201250511"), Is.True);
            Assert.That(result.Contains("3211057617"), Is.True);
            Assert.That(result.Contains("3214532138"), Is.True);
            Assert.That(result.Contains("3215734699"), Is.True);
            Assert.That(result.Contains("3762427879"), Is.True);
        }

        [Test]
        public async Task GetNhsNumbersFromCsv()
        {
            // Arrange
            var physicalFile = new FileInfo("TestFiles/test.csv");
            IFormFile formFile = physicalFile.AsMockIFormFile();

            // Act
            NhsOptOut.Util.TextReader tr = new NhsOptOut.Util.TextReader();
            List<string> result = await tr.GetNhsNumbers(formFile);

            // Assert
            Assert.That(result.Count, Is.EqualTo(4));
            Assert.That(result.Contains("5749859944"), Is.True);
            Assert.That(result.Contains("4164939559"), Is.True);
            Assert.That(result.Contains("4108270991"), Is.True);
            Assert.That(result.Contains("9999999999"), Is.True);
        }


    }
}