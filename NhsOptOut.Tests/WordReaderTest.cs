﻿using Microsoft.AspNetCore.Http;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Threading.Tasks;

namespace NhsOptOut.Tests
{
    [TestFixture]
    public class WordReaderTest
    {
        [Test]
        public async Task GetNhsNumbersFromText()
        {
            // Arrange
            var physicalFile = new FileInfo("TestFiles/test.docx");
            IFormFile formFile = physicalFile.AsMockIFormFile();

            // Act
            try
            {
                NhsOptOut.Util.WordReader wr = new NhsOptOut.Util.WordReader();
                List<string> result = await wr.GetNhsNumbers(formFile);

                // Assert
                Assert.That(result.Count, Is.EqualTo(4));
                Assert.That(result.Contains("3215734699 "), Is.True);
                Assert.That(result.Contains("3215902257"), Is.True);
                Assert.That(result.Contains("3223219021"), Is.True);
                Assert.That(result.Contains("323015962"), Is.True);
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex);
            }

        }
    }
}
