﻿using Microsoft.AspNetCore.Http;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Threading.Tasks;

namespace NhsOptOut.Tests
{
    [TestFixture]
    public class ExcelReaderTest
    {
        [Test]
        public async Task GetNhsNumbersFromExcel()
        {
            // Arrange
            var physicalFile = new FileInfo("TestFiles/test.xlsx");
            IFormFile formFile = physicalFile.AsMockIFormFile();

            // Act
            try
            {
                NhsOptOut.Util.ExcelReader er = new NhsOptOut.Util.ExcelReader();
                List<string> result = await er.GetNhsNumbers(formFile);

                // Assert
                Assert.That(result.Count, Is.EqualTo(5));
                Assert.That(result.Contains("3201250511 "), Is.True);
                Assert.That(result.Contains("3214532138"), Is.True);
                Assert.That(result.Contains("3211057617"), Is.True);
                Assert.That(result.Contains("3225834432"), Is.True);
                Assert.That(result.Contains("3269784503"), Is.True);
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex);
            }

        }

        [Test]
        public async Task GetNhsNumbersFromExcel2()
        {
            // Arrange
            var physicalFile = new FileInfo("TestFiles/test2.xlsx");
            IFormFile formFile = physicalFile.AsMockIFormFile();

            // Act
            try
            {
                NhsOptOut.Util.ExcelReader er = new NhsOptOut.Util.ExcelReader();
                List<string> result = await er.GetNhsNumbers(formFile);

                // Assert
                Assert.That(result.Count, Is.EqualTo(232));
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex);
            }

        }

    }
}
