﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

namespace NhsOptOut.Models
{
    public partial class MPIContext : DbContext
    {
        public MPIContext()
        {
        }

        public MPIContext(DbContextOptions<MPIContext> options)
            : base(options)
        {
        }

        public virtual DbSet<Mpifull> Mpifull { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            //if (!optionsBuilder.IsConfigured)
            //{
            //}
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Mpifull>(entity =>
            {
                entity.HasNoKey();

                entity.ToTable("MPIFull");

                entity.HasIndex(e => new { e.PatientNumber, e.DateOfDeath })
                    .HasName("IX_MPIFull_Unactioned");

                entity.HasIndex(e => new { e.PatientNumber, e.Add1, e.Add2, e.Religion, e.PostCode })
                    .HasName("YDH_Postcode");

                entity.HasIndex(e => new { e.PatientNumber, e.Surname, e.Forename, e.DateOfBirth, e.Religion, e.TraceStatus })
                    .HasName("YDH-ClusteredIdx")
                    .IsClustered();

                entity.Property(e => e.Add1)
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.Add2)
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.Add3)
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.AddressCityCode1)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.AddressCityCode2)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.AddressCityCode3)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.AddressCityCode4)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.AddressCityCode5)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.AddressCityDescription1)
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.AddressCityDescription2)
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.AddressCityDescription3)
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.AddressCityDescription4)
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.AddressCityDescription5)
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.AddressCountryCode1)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.AddressCountryCode2)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.AddressCountryCode3)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.AddressCountryCode4)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.AddressCountryCode5)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.AddressCountryDescription1)
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.AddressCountryDescription2)
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.AddressCountryDescription3)
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.AddressCountryDescription4)
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.AddressCountryDescription5)
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.AddressCountyCode1)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.AddressCountyCode2)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.AddressCountyCode3)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.AddressCountyCode4)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.AddressCountyCode5)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.AddressCountyDescription1)
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.AddressCountyDescription2)
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.AddressCountyDescription3)
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.AddressCountyDescription4)
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.AddressCountyDescription5)
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.AddressDateFrom1)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.AddressDateFrom2)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.AddressDateFrom3)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.AddressDateFrom4)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.AddressDateFrom5)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.AddressLineA1)
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.AddressLineA2)
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.AddressLineA3)
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.AddressLineA4)
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.AddressLineA5)
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.AddressLineC1)
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.AddressLineC2)
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.AddressLineC3)
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.AddressLineC4)
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.AddressLineC5)
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.AddressPostCode1)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.AddressPostCode2)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.AddressPostCode3)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.AddressPostCode4)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.AddressPostCode5)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.AddressRegionCode1)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.AddressRegionCode2)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.AddressRegionCode3)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.AddressRegionCode4)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.AddressRegionCode5)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.AddressRegionsDescription1)
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.AddressRegionsDescription2)
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.AddressRegionsDescription3)
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.AddressRegionsDescription4)
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.AddressRegionsDescription5)
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.AliasActiveCode1)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.AliasActiveCode10)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.AliasActiveCode11)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.AliasActiveCode2)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.AliasActiveCode3)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.AliasActiveCode4)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.AliasActiveCode5)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.AliasActiveCode6)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.AliasActiveCode7)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.AliasActiveCode8)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.AliasActiveCode9)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.AliasChangeDate1)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.AliasChangeDate10)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.AliasChangeDate11)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.AliasChangeDate2)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.AliasChangeDate3)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.AliasChangeDate4)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.AliasChangeDate5)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.AliasChangeDate6)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.AliasChangeDate7)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.AliasChangeDate8)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.AliasChangeDate9)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.AliasChangeTime1)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.AliasChangeTime10)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.AliasChangeTime11)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.AliasChangeTime2)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.AliasChangeTime3)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.AliasChangeTime4)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.AliasChangeTime5)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.AliasChangeTime6)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.AliasChangeTime7)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.AliasChangeTime8)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.AliasChangeTime9)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.AliasDateOfBirth1)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.AliasDateOfBirth10)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.AliasDateOfBirth11)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.AliasDateOfBirth2)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.AliasDateOfBirth3)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.AliasDateOfBirth4)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.AliasDateOfBirth5)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.AliasDateOfBirth6)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.AliasDateOfBirth7)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.AliasDateOfBirth8)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.AliasDateOfBirth9)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.AliasFreeText1)
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.AliasFreeText10)
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.AliasFreeText11)
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.AliasFreeText2)
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.AliasFreeText3)
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.AliasFreeText4)
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.AliasFreeText5)
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.AliasFreeText6)
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.AliasFreeText7)
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.AliasFreeText8)
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.AliasFreeText9)
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.AliasFullHl7text1)
                    .HasColumnName("AliasFullHL7Text1")
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.AliasFullHl7text10)
                    .HasColumnName("AliasFullHL7Text10")
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.AliasFullHl7text11)
                    .HasColumnName("AliasFullHL7Text11")
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.AliasFullHl7text2)
                    .HasColumnName("AliasFullHL7Text2")
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.AliasFullHl7text3)
                    .HasColumnName("AliasFullHL7Text3")
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.AliasFullHl7text4)
                    .HasColumnName("AliasFullHL7Text4")
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.AliasFullHl7text5)
                    .HasColumnName("AliasFullHL7Text5")
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.AliasFullHl7text6)
                    .HasColumnName("AliasFullHL7Text6")
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.AliasFullHl7text7)
                    .HasColumnName("AliasFullHL7Text7")
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.AliasFullHl7text8)
                    .HasColumnName("AliasFullHL7Text8")
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.AliasFullHl7text9)
                    .HasColumnName("AliasFullHL7Text9")
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.AliasInternalRowid1)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.AliasInternalRowid10)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.AliasInternalRowid11)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.AliasInternalRowid2)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.AliasInternalRowid3)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.AliasInternalRowid4)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.AliasInternalRowid5)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.AliasInternalRowid6)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.AliasInternalRowid7)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.AliasInternalRowid8)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.AliasInternalRowid9)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.AliasSurname1)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.AliasSurname10)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.AliasSurname11)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.AliasSurname2)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.AliasSurname3)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.AliasSurname4)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.AliasSurname5)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.AliasSurname6)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.AliasSurname7)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.AliasSurname8)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.AliasSurname9)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.AliasType1)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.AliasType10)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.AliasType11)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.AliasType2)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.AliasType3)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.AliasType4)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.AliasType5)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.AliasType6)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.AliasType7)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.AliasType8)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.AliasType9)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.DateOfBirth).HasColumnType("date");

                entity.Property(e => e.DateOfDeath).HasColumnType("date");

                entity.Property(e => e.EthnicGroup)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Forename)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Gender)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Gpaddress1)
                    .HasColumnName("GPAddress1")
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.Gpaddress2)
                    .HasColumnName("GPAddress2")
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.Gpaddress3)
                    .HasColumnName("GPAddress3")
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.Gpaddress4)
                    .HasColumnName("GPAddress4")
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.Gpaddress5)
                    .HasColumnName("GPAddress5")
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.Gpccgcode1)
                    .HasColumnName("GPCCGCode1")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Gpccgcode2)
                    .HasColumnName("GPCCGCode2")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Gpccgcode3)
                    .HasColumnName("GPCCGCode3")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Gpccgcode4)
                    .HasColumnName("GPCCGCode4")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Gpccgcode5)
                    .HasColumnName("GPCCGCode5")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Gpccgdesc1)
                    .HasColumnName("GPCCGDesc1")
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.Gpccgdesc2)
                    .HasColumnName("GPCCGDesc2")
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.Gpccgdesc3)
                    .HasColumnName("GPCCGDesc3")
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.Gpccgdesc4)
                    .HasColumnName("GPCCGDesc4")
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.Gpccgdesc5)
                    .HasColumnName("GPCCGDesc5")
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.Gpcode1)
                    .HasColumnName("GPCode1")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Gpcode2)
                    .HasColumnName("GPCode2")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Gpcode3)
                    .HasColumnName("GPCode3")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Gpcode4)
                    .HasColumnName("GPCode4")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Gpcode5)
                    .HasColumnName("GPCode5")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.GpdateFrom1)
                    .HasColumnName("GPDateFrom1")
                    .HasColumnType("date");

                entity.Property(e => e.GpdateFrom2)
                    .HasColumnName("GPDateFrom2")
                    .HasColumnType("date");

                entity.Property(e => e.GpdateFrom3)
                    .HasColumnName("GPDateFrom3")
                    .HasColumnType("date");

                entity.Property(e => e.GpdateFrom4)
                    .HasColumnName("GPDateFrom4")
                    .HasColumnType("date");

                entity.Property(e => e.GpdateFrom5)
                    .HasColumnName("GPDateFrom5")
                    .HasColumnType("date");

                entity.Property(e => e.Gpdesc1)
                    .HasColumnName("GPDesc1")
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.Gpdesc2)
                    .HasColumnName("GPDesc2")
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.Gpdesc3)
                    .HasColumnName("GPDesc3")
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.Gpdesc4)
                    .HasColumnName("GPDesc4")
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.Gpdesc5)
                    .HasColumnName("GPDesc5")
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.Gpgmc1)
                    .HasColumnName("GPGMC1")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Gpgmc2)
                    .HasColumnName("GPGMC2")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Gpgmc3)
                    .HasColumnName("GPGMC3")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Gpgmc4)
                    .HasColumnName("GPGMC4")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Gpgmc5)
                    .HasColumnName("GPGMC5")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Gpgroup1)
                    .HasColumnName("GPGroup1")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Gpgroup2)
                    .HasColumnName("GPGroup2")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Gpgroup3)
                    .HasColumnName("GPGroup3")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Gpgroup4)
                    .HasColumnName("GPGroup4")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Gpgroup5)
                    .HasColumnName("GPGroup5")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.GppracticeCode1)
                    .HasColumnName("GPPracticeCode1")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.GppracticeCode2)
                    .HasColumnName("GPPracticeCode2")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.GppracticeCode3)
                    .HasColumnName("GPPracticeCode3")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.GppracticeCode4)
                    .HasColumnName("GPPracticeCode4")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.GppracticeCode5)
                    .HasColumnName("GPPracticeCode5")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.GppracticeDesc1)
                    .HasColumnName("GPPracticeDesc1")
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.GppracticeDesc2)
                    .HasColumnName("GPPracticeDesc2")
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.GppracticeDesc3)
                    .HasColumnName("GPPracticeDesc3")
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.GppracticeDesc4)
                    .HasColumnName("GPPracticeDesc4")
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.GppracticeDesc5)
                    .HasColumnName("GPPracticeDesc5")
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.HealthCareAreaCode)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.HealthCareAreaDescription)
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.HomePhone)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.MaritalStatus)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.NationalId)
                    .HasColumnName("NationalID")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.OverseasVisitorStatusCodeLastEpisode)
                    .HasColumnName("OverseasVisitorStatusCode(LastEpisode)")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.OverseasVisitorStatusDescriptionLastEpisode)
                    .HasColumnName("OverseasVisitorStatusDescription(LastEpisode)")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.PaperRowid)
                    .HasColumnName("PAPER_Rowid")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.PapmiActive)
                    .HasColumnName("PAPMI_Active")
                    .HasMaxLength(3)
                    .IsUnicode(false);

                entity.Property(e => e.PatientNumber)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.PostCode)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.PreferredLanuguage)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Region)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.RegisteredGpccgcode)
                    .HasColumnName("RegisteredGPCCGCode")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.RegisteredGpccgname)
                    .HasColumnName("RegisteredGPCCGName")
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.RegisteredGpcode)
                    .HasColumnName("RegisteredGPcode")
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.RegisteredGpname)
                    .HasColumnName("RegisteredGPname")
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.RegisteredGppracticeCode)
                    .HasColumnName("RegisteredGPPracticeCode")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.RegisteredGppracticename)
                    .HasColumnName("RegisteredGPPracticename")
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.Religion)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Surname)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.TimeOfDeath)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.TitleSalutation)
                    .HasColumnName("Title Salutation")
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.TraceStatus)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.UpdateDate).HasColumnType("date");

                entity.Property(e => e.WorkPhone)
                    .HasMaxLength(50)
                    .IsUnicode(false);
            });

            OnModelCreatingPartial(modelBuilder);
        }

        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
    }
}
