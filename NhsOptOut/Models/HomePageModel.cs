﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NhsOptOut.Models
{
    public class HomePageModel
    {
        public string nhsNumber { get; set; }
        public IFormFile file { get; set; }
    }
}
