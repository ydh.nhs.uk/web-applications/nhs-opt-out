﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NhsOptOut.Models
{
    // To be read from appsettings file - currently unused
    public class OptOutOptions
    {
        public OptOutOptions() { }

        public string Setting1 { get; set; }
        public string Setting2 { get; set; }
    }
}
