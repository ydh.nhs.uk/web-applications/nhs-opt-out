﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NhsOptOut.Models
{
    public class LookupResult
    {
        public LookupResult() { }

        public LookupResult(VwOptOutStatus status, DateTime lastLoadDate)
        {
            this.nhsNumber = status.NationalId;
            this.optout = (status.Status == "OPTOUT");
            // If the patient has opted out, their number won't be in the MESHReturn table
            // so there's no "load date". Instead, use the max load date from that table
            this.loadDate = status.LoadDate ?? lastLoadDate;
            this.cacheDate = status.CacheDate;
            notFound = false;
        }

        public string nhsNumber { get; set; }
        public bool optout { get; set; }
        public bool notFound { get; set; }
        public DateTime? loadDate { get; set; }
        public DateTime? cacheDate { get; set; }
        public string initials { get; set; }
    }
}
