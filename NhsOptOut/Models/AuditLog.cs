﻿using System;
using System.Collections.Generic;

namespace NhsOptOut.Models
{
    public partial class AuditLog
    {
        public int Id { get; set; }
        public string Username { get; set; }
        public DateTime DateAccessed { get; set; }
        public int LookupCount { get; set; }
    }
}
