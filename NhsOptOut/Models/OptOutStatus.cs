﻿using System;
using System.Collections.Generic;

namespace NhsOptOut.Models
{
    public partial class OptOutStatus
    {
        public string NationalId { get; set; }
        public DateTime? CacheDate { get; set; }
        public bool? OptOutStatus1 { get; set; }
    }
}
