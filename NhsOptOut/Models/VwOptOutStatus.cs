﻿using System;
using System.Collections.Generic;

namespace NhsOptOut.Models
{
    public partial class VwOptOutStatus
    {
        public string NationalId { get; set; }
        public string Status { get; set; }
        public DateTime? LoadDate { get; set; }
        public DateTime? CacheDate { get; set; }
    }
}
