﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

namespace NhsOptOut.Models
{
    public partial class nationalOptOutContext : DbContext
    {
        public nationalOptOutContext()
        {
        }

        public nationalOptOutContext(DbContextOptions<nationalOptOutContext> options)
            : base(options)
        {
        }

        public virtual DbSet<AuditLog> AuditLog { get; set; }
        public virtual DbSet<Meshlog> Meshlog { get; set; }
        public virtual DbSet<Meshreturn> Meshreturn { get; set; }
        public virtual DbSet<OptOutStatus> OptOutStatus { get; set; }
        public virtual DbSet<VwOptOutStatus> VwOptOutStatus { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
                //optionsBuilder.UseSqlServer("connection string....");
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<AuditLog>(entity =>
            {
                entity.Property(e => e.DateAccessed).HasColumnType("datetime");

                entity.Property(e => e.Username)
                    .IsRequired()
                    .HasMaxLength(50);
            });

            modelBuilder.Entity<Meshlog>(entity =>
            {
                entity.ToTable("MESHLog");

                entity.Property(e => e.Description).HasMaxLength(250);

                entity.Property(e => e.Event)
                    .IsRequired()
                    .HasMaxLength(50);

                entity.Property(e => e.Filename).HasMaxLength(50);

                entity.Property(e => e.Meshdate)
                    .HasColumnName("MESHDate")
                    .HasColumnType("datetime");

                entity.Property(e => e.Status)
                    .IsRequired()
                    .HasMaxLength(50);
            });

            modelBuilder.Entity<Meshreturn>(entity =>
            {
                entity.HasNoKey();

                entity.ToTable("MESHReturn");

                entity.Property(e => e.LoadDate).HasColumnType("datetime");

                entity.Property(e => e.Meshout)
                    .IsRequired()
                    .HasColumnName("MESHOut")
                    .HasMaxLength(10)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<OptOutStatus>(entity =>
            {
                entity.HasNoKey();

                entity.Property(e => e.CacheDate).HasColumnType("datetime");

                entity.Property(e => e.NationalId)
                    .HasColumnName("NationalID")
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.OptOutStatus1).HasColumnName("OptOutStatus");
            });

            modelBuilder.Entity<VwOptOutStatus>(entity =>
            {
                entity.HasNoKey();

                entity.ToView("vw_OptOutStatus");

                entity.Property(e => e.CacheDate).HasColumnType("datetime");

                entity.Property(e => e.LoadDate).HasColumnType("datetime");

                entity.Property(e => e.NationalId)
                    .HasColumnName("NationalID")
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.Status)
                    .HasMaxLength(10)
                    .IsUnicode(false);
            });

            OnModelCreatingPartial(modelBuilder);
        }

        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
    }
}
