﻿using System;
using System.Collections.Generic;

namespace NhsOptOut.Models
{
    public partial class Meshreturn
    {
        public string Meshout { get; set; }
        public DateTime? LoadDate { get; set; }
    }
}
