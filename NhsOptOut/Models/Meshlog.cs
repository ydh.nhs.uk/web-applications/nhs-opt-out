﻿using System;
using System.Collections.Generic;

namespace NhsOptOut.Models
{
    public partial class Meshlog
    {
        public int Id { get; set; }
        public DateTime Meshdate { get; set; }
        public string Event { get; set; }
        public string Status { get; set; }
        public string Description { get; set; }
        public string Filename { get; set; }
    }
}
