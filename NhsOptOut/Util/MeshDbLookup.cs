﻿using NhsOptOut.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NhsOptOut.Util
{
    public class MeshDbLookup
    {
        public static LookupResult Lookup(nationalOptOutContext db, string nhsNumber)
        {
            DateTime lastLoadDate = GetLastLoadDate(db);
            LookupResult result = db.VwOptOutStatus
                                    .Where(w => w.NationalId == nhsNumber)
                                    .Select(s => new LookupResult(s, lastLoadDate))
                                    .SingleOrDefault();
            if (result == null)
            {
                result = new LookupResult { nhsNumber = nhsNumber, notFound = true };
            }

            return result;
        }

        public static List<LookupResult> LookupMultiple(nationalOptOutContext db, List<string> nhsNumbers)
        {
            DateTime lastLoadDate = GetLastLoadDate(db);
            List<LookupResult> result = db.VwOptOutStatus
                                    .Where(w => nhsNumbers.Contains(w.NationalId))
                                    .Select(s => new LookupResult(s, lastLoadDate))
                                    .ToList();

            var recordsNotFound = nhsNumbers
                                    .Where(w => !result.Any(a => a.nhsNumber == w))
                                    .Select(s => new LookupResult { nhsNumber = s, notFound = true });
            result.AddRange(recordsNotFound);

            return result;
        }

        protected static DateTime GetLastLoadDate(nationalOptOutContext db)
        {
            return db.Meshreturn.Max(m => m.LoadDate) ?? DateTime.MinValue;
        }
    }
}
