﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace NhsOptOut.Util
{
    public class TextReader : IContentReader
    {
        // Lookbehind: Not preceded by a number
        // Match: Optional space + 10 numbers, optional spaces after 3rd and 6th digit
        // Lookahead: Not followed by another number
        public static Regex rgx = new Regex("(?<![0-9])[ ]{0,1}[0-9]{3}[ ]{0,1}[0-9]{3}[ ]{0,1}[0-9]{4}(?![0-9])", RegexOptions.Compiled);

        public async Task<List<string>> GetNhsNumbers(IFormFile file)
        {
            List<string> nhsNumbers = new List<string>();

            // TODO - check MIME type

            string fileContent;
            using (var stream = file.OpenReadStream())
            using (var reader = new StreamReader(stream))
            {
                fileContent = await reader.ReadToEndAsync();
            }

            var matches = rgx.Matches(fileContent);

            foreach (Match match in matches)
            {
                string nhsNumber = match.Value.Replace(" ", "");
                if (!nhsNumbers.Contains(nhsNumber))
                {
                    nhsNumbers.Add(nhsNumber);
                }
            }

            return nhsNumbers;
        }
    }
}
