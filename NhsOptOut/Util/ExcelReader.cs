﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using OfficeOpenXml;

namespace NhsOptOut.Util
{
    public class ExcelReader : IContentReader
    {
        public static Regex rgx = new Regex("^[0-9]{3}[ ]{0,1}[0-9]{3}[ ]{0,1}[0-9]{4}$", RegexOptions.Compiled);
        public static Regex rgx2 = new Regex("(?<![0-9])[ ]{0,1}[0-9]{3}[ ]{0,1}[0-9]{3}[ ]{0,1}[0-9]{4}(?![0-9])", RegexOptions.Compiled);

        public async Task<List<string>> GetNhsNumbers(IFormFile file)
        {
            // TODO - check MIME type

            // New for EPPlus v5 - set the license type to non-commercial
            ExcelPackage.LicenseContext = LicenseContext.NonCommercial;

            List<string> nhsNumbers = new List<string>();

            using (var stream = file.OpenReadStream())
            {
                using (var pck = new ExcelPackage(stream))
                {
                    foreach (var worksheet in pck.Workbook.Worksheets)
                    {
                        //Cells only contains references to cells with actual data
                        var cells = worksheet.Cells;

                        foreach (var cell in cells)
                        {
                            if (cell.Value != null)
                            {
                                string cellValue = cell.Value.ToString() ?? "";
                                // See if the whole cell is an NHS number
                                var match1 = rgx.Match(cellValue);
                                if (match1 != Match.Empty)
                                {
                                    nhsNumbers.Add(cellValue);
                                }
                                else
                                {
                                    // See if the cell *contains* an NHS number
                                    var match2 = rgx2.Match(cellValue);
                                    if (match2 != Match.Empty)
                                    {
                                        var nhsNumber = match2.Value.Replace(" ", "");
                                        nhsNumbers.Add(nhsNumber);
                                    }
                                }
                            }
                        }
                    }
                }
            }
           
            return nhsNumbers;
        }

    }
}
