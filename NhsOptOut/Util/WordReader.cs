﻿using DocumentFormat.OpenXml.Packaging;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Xml;

namespace NhsOptOut.Util
{
    public class WordReader : IContentReader
    {
        // Lookbehind: Not preceded by a number
        // Match: Optional space + 10 numbers, optional spaces after 3rd and 6th digit
        // Lookahead: Not followed by another number
        public static Regex rgx = new Regex("(?<![0-9])[ ]{0,1}[0-9]{3}[ ]{0,1}[0-9]{3}[ ]{0,1}[0-9]{4}(?![0-9])", RegexOptions.Compiled);

        public async Task<List<string>> GetNhsNumbers(IFormFile file)
        {
            List<string> nhsNumbers = new List<string>();

            // TODO - check MIME type

            const string wordmlNamespace = "http://schemas.openxmlformats.org/wordprocessingml/2006/main";

            StringBuilder textBuilder = new StringBuilder();
            using (var stream = file.OpenReadStream())
            using (WordprocessingDocument wdDoc = WordprocessingDocument.Open(stream, false))
            {
                // Manage namespaces to perform XPath queries.  
                NameTable nt = new NameTable();
                XmlNamespaceManager nsManager = new XmlNamespaceManager(nt);
                nsManager.AddNamespace("w", wordmlNamespace);

                // Get the document part from the package.  
                // Load the XML in the document part into an XmlDocument instance.  
                XmlDocument xdoc = new XmlDocument(nt);
                xdoc.Load(wdDoc.MainDocumentPart.GetStream());

                XmlNodeList paragraphNodes = xdoc.SelectNodes("//w:p", nsManager);
                foreach (XmlNode paragraphNode in paragraphNodes)
                {
                    XmlNodeList textNodes = paragraphNode.SelectNodes(".//w:t", nsManager);
                    foreach (System.Xml.XmlNode textNode in textNodes)
                    {
                        textBuilder.Append(textNode.InnerText);
                    }
                    textBuilder.Append(Environment.NewLine);
                }

            }
            string fileContent = textBuilder.ToString();

            var matches = rgx.Matches(fileContent);

            foreach (Match match in matches)
            {
                string nhsNumber = match.Value.Replace(" ", "");
                if (!nhsNumbers.Contains(nhsNumber))
                {
                    nhsNumbers.Add(nhsNumber);
                }
            }

            return nhsNumbers;
        }
    }
}
