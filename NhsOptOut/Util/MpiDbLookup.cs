﻿using NhsOptOut.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NhsOptOut.Util
{
    public class MpiDbLookup
    {
        public static void AddPatientInitials(MPIContext mpiDb, List<LookupResult> results)
        {
            List<string> nhsNumbers = results.Where(w => !w.notFound).Select(s => s.nhsNumber).ToList();

            var allNames = mpiDb.Mpifull
                        .Where(w => nhsNumbers.Contains(w.NationalId))
                        .Select(s => new Initials
                        {
                            firstInitial = s.Forename.Substring(0, 1).ToUpper(),
                            lastInitial = s.Surname.Substring(0, 1).ToUpper(),
                            nhsNumber = s.NationalId,
                            updateDate = s.UpdateDate
                        })
                        .ToList();

            foreach (var result in results)
            {
                // If > 1 record for this patient, use the one with the latest update date
                var thisName = allNames.Where(w => w.nhsNumber == result.nhsNumber).OrderByDescending(o => o.updateDate).FirstOrDefault();
                if (thisName != null)
                {
                    result.initials = thisName.firstInitial + thisName.lastInitial;
                }
            }
        }
    }

    public class Initials
    {
        public DateTime? updateDate { get; set; }
        public string firstInitial { get; set; }
        public string lastInitial { get; set; }
        public string nhsNumber { get; set; }
    }

}
