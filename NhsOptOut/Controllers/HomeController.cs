﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Diagnostics;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using NhsOptOut.Models;
using NhsOptOut.Util;
using TextReader = NhsOptOut.Util.TextReader;

namespace NhsOptOut.Controllers
{
    [Authorize]
    public class HomeController : Controller
    {
        private readonly ILogger<HomeController> _logger;
        private readonly IConfiguration _config;
        private nationalOptOutContext _db;
        private MPIContext _mpiDb;
        private IOptionsSnapshot<OptOutOptions> _options;
        private static readonly List<string> TEXT_TYPES = new List<string> { ".txt", ".csv" };
        private static readonly List<string> EXCEL_TYPES = new List<string> { ".xlsx" };
        private static readonly List<string> WORD_TYPES = new List<string> { ".docx" };

        public HomeController(ILogger<HomeController> logger, IConfiguration config, nationalOptOutContext db, MPIContext mpiDb, IOptionsSnapshot<OptOutOptions> subOptionsAccessor)
        {
            _db = db;
            _mpiDb = mpiDb;
            _config = config;
            _options = subOptionsAccessor;

            var x = _db.Meshlog.First();

            _logger = logger;
        }

        public IActionResult Index()
        {
            return View(new HomePageModel());
        }

        [HttpPost]
        public async Task<IActionResult> Index(HomePageModel model)
        {
            string user = HttpContext.User?.Identity?.Name ?? "Unknown";
            List<LookupResult> results = null;

            if (!String.IsNullOrEmpty(model.nhsNumber))
            {
                // Single NHS number
                _logger.LogDebug($"{user} requesting single NHS number");
                results = new List<LookupResult>();
                results.Add(MeshDbLookup.Lookup(_db, model.nhsNumber));
            }
            else if (model.file != null)
            {
                // File upload
                string ext = Path.GetExtension(model.file.FileName).ToLower();
                string contentType = model.file.ContentType;
                _logger.LogDebug($"{user} uploaded {ext} file");

                // Check if the file type is supported
                IContentReader reader = GetReaderForFileType(ext);

                if (reader == null)
                {
                    ModelState.AddModelError("file", "Invalid file type!");
                    return View(new HomePageModel());
                }

                try
                {
                    List<string> nhsNumbers = await reader.GetNhsNumbers(model.file);
                    results = MeshDbLookup.LookupMultiple(_db, nhsNumbers);
                }
                catch (Exception ex)
                {
                    _logger.LogError(ex, "Error processing NHS numbers");
                    ViewData["ErrorMessage"] = "There was an error processing the file";
                }

                if (results == null) results = new List<LookupResult>();

            }

            // Add patient initials to each record - look up their entry in the MPI database
            MpiDbLookup.AddPatientInitials(_mpiDb, results);

            // Create an entry in the AuditLog table
            AuditLog logEntry = new AuditLog { 
                                    DateAccessed = DateTime.UtcNow, 
                                    LookupCount = results.Count, 
                                    Username = user
            };
            _db.AuditLog.Add(logEntry);
            _db.SaveChanges();
            _logger.LogDebug($"Returning {results.Count} results");
            
            return View("Result", results);
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            var error = HttpContext
                  .Features
                  .Get<IExceptionHandlerFeature>();

            if (error != null)
                _logger.LogError(error.Error, "An exception occurred");

            return View(new ErrorViewModel { RequestId = System.Diagnostics.Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }

        protected IContentReader GetReaderForFileType(string extension)
        {
            if (TEXT_TYPES.Contains(extension))
            {
                return new TextReader();
            }
            else if (EXCEL_TYPES.Contains(extension))
            {
                return new ExcelReader();
            }
            else if (WORD_TYPES.Contains(extension))
            {
                return new WordReader();
            }
            else
            {
                return null;
            }
        }
    }
}
